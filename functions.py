"""
I use some legasy code from site, like class PerfectMoney and some functions
for more "pythonic" code everything needs to be put inside the class, but I do not have time for this now
I'll do it a little later, sorry
"""

from pm_base import PerfectMoney
from local_settings import *
from optparse import OptionParser
# for write data from balance and read in rtancfer
import pickle

user = {'account': ACCOUNT ,'password' : PASSWORD} # in local settings ACCOUNT = 'your account' PASSWORD = 'your password'

def print_history(account, password, startmonth, startday, startyear, endmonth, endday, endyear):
    pm = PerfectMoney(account, password)
    res = pm.history(startmonth, startday, startyear, endmonth, endday, endyear)
    if pm.error:
        print pm.error
        return
    print res
    return res

def get_balance(account, password):
    pm = PerfectMoney(account, password)
    res = pm.balance()
    pickle.dump(res, open("balance.txt", 'w'))
    if pm.error:
        print pm.error
        return
    print res
    return res

def create_ev(account, password, payer, amount):
    pm = PerfectMoney(account, password)
    res = pm.ev_create(payer, amount)
    if pm.error:
        print pm.error
        return
    print res

def get_by_id(account, password, id):
    pm = PerfectMoney(account, password)
    res = pm.get_spend_by_id(id)
    if pm.error:
        print pm.error
        return
    print res
    return res

def transfer_money(account, password, payer, payee, amount, memo, payment_id):
    # checking for the necessary amount is performed automatically in the API
    f=pickle.load(open("balance.txt"))
    transfer_error = f[payer] >= amount
    pm = PerfectMoney(account, password)
    if not transfer_error:
        res = pm.transfer(payer, payee, amount, memo, payment_id)
        if pm.error:
            print pm.error
            return
        print res
        return res
    else:
        print 'not enough money'
        return 'not enough money'

def myPM(user, action, param): #Fixme: need make the transfer of parameters better

    if action =='balance':
        res = get_balance(user['account'], user['password'])
        return res

    elif action == 'status':
        res = get_by_id(user['account'], user['password'], param['PAYMENT_ID'])
        return res

    elif action == 'transfer':
        res = transfer_money(
            user['account'],
            user['password'],
            param['Payer_Account'],
            param['Payee_Account'],
            param['Amount'],
            param['Memo'],
            param['PAYMENT_ID'])
        return res

if __name__ == '__main__':

    parser = OptionParser(conflict_handler="resolve")
    parser.add_option('-a','--action', action='store', choices=['balance','transfer','status'], dest='action', default=None, help='Set action')
    parser.add_option('-u', '--user', action='store', dest='user', nargs=2, help = 'Get user with login and password')
    parser.add_option('-p', '--param', action='store', dest='parameters', nargs=5, help = 'Get parameters : Payer_Account, Payee_Account, Amount, Memo, PAYMENT_ID ')
    options,arguments = parser.parse_args()

    # if user did not enter the parameters
    if not options.__dict__['action'] or not options.__dict__['user']:
        param = {}
        print 'Hello this is test program with using PerfectMoney.\n ' \
              'If you whant get you balance - type "balance". \n ' \
              'If you whant payment status with id - type "status".\n ' \
              'If you whant send money? type "transfer". \n ' \
              'If you whant quit - type "quit"! \n'

        if not user['account'] or not user['password']:
            # print 'Who are you?'
            user['account'] = raw_input('Account: ')
            user['password'] = raw_input('Password: ')

        action = raw_input ('What do you need ? ')

        if action in ['quit', 'Quit', 'QUIT']:

            print 'Good bye!'

        elif action == 'balance':
            # fixme: checking for the necessary amount is performed automatically in the API
            # But i still need to implement writing data to file (upd do it in func)
            myPM(user, action, None)

        elif action =='status':

            param['PAYMENT_ID'] = raw_input('Spend ID? ')
            myPM(user, action, param)

        elif action == 'transfer':

            param['Payer_Account'] = raw_input('Payer ? ')
            param['Payee_Account'] = raw_input('Payee ? ')
            param['Amount'] = raw_input('Amount ? ')
            param['Memo'] = raw_input('Description ? ')
            param['PAYMENT_ID'] = raw_input('Payment_ID ? ')
            myPM(user, action, param)

        else:
            print 'I d\'nt anderstand. Try again!'

    # if user enter the parameters
    else :
        user['account'] = options.__dict__['user'][0]
        user['password'] = options.__dict__['user'][1]
        action = options.__dict__['action']
        param = {}

        if  action == 'balance':
            # fixme: checking for the necessary amount is performed automatically in the API
            # But i still need to implement writing data to file
            myPM(user, action, None)

        elif action =='status':
            param['PAYMENT_ID'] =  options.__dict__['parameters'][0]
            myPM(user, action, param)

        elif action == 'transfer':

            param['Payer_Account'] = options.__dict__['parameters'][0]
            param['Payee_Account'] = options.__dict__['parameters'][1]
            param['Amount'] = options.__dict__['parameters'][2]
            param['Memo'] = options.__dict__['parameters'][3]
            param['PAYMENT_ID'] = options.__dict__['parameters'][4]
            myPM(user, action, param)

