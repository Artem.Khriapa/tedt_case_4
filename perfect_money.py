from functions import  get_balance, get_by_id
from functions import user as myUser

def P_M():
    user = myUser
    # user = {
    # 'account': '',
    # 'password' : ''
    # }
    print 'Hello this is test program with using PerfectMoney.\n ' \
          'If you whant get you balance - type "balance". \n ' \
          'If you whant payment status with id - type "status".\n ' \
          'If you whant quit - type "quit"! \n'
    while True:
        if not user['account'] or not user['password']:
            print 'Who are you?'
            user['account'] = raw_input('Account: ')
            user['password'] = raw_input('Password: ')

        user_input = raw_input ('What do you need ? ')

        if  user_input in ['quit','Quit','QUIT']:

            print 'Good bye!'
            break

        elif user_input in ['balance','Balance','BALANCE']:

            get_balance(user['account'], user['password'])

        elif user_input in ['status','Status','STATUS']:

            spend_id = raw_input('Spend ID? ')
            get_by_id(user['account'], user['password'],spend_id)

        else : print 'I d\'nt anderstand. Try again!'

if __name__ == '__main__':
    P_M()